package com.lagou;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.mapper.ArticleMapper;
import com.lagou.pojo.Article;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootZuoyeApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private ArticleMapper articleMapper;

	@Test
	public void  test(){
		Article article = articleMapper.selectById(1);
		System.out.println(article);
	}

	@Test
	public void  testPage(){
		QueryWrapper<Article> wrapper = new QueryWrapper();
		Page<Article> page = new Page<Article>(1,2);
		page = articleMapper.selectPage(page, null);
		List<Article> records = page.getRecords();
		for (int i = 0; i < records.size(); i++) {
			System.out.println(records.get(i));
		}
		System.out.println(page.getTotal());
	}
}
