package com.lagou.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.mapper.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class ArticleController {
    @Autowired
    private ArticleMapper articleMapper;

    @RequestMapping("/")
    public String index(Map<String,Object> map,
                        @RequestParam(defaultValue = "2") long size, @RequestParam(defaultValue = "1") long current){
        Page page = new Page(current,size);
        map.put("page",articleMapper.selectPage(page,null));
        return "client/index";
    }
}
