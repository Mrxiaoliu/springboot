package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootZuoyeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootZuoyeApplication.class, args);
	}
}
