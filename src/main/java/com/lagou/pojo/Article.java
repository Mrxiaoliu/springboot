package com.lagou.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_article")
public class Article {
    private Long id;//id
    private String title;//标题
    private String content;//文章具体内容
    private Date created;//发表时间
    private Date modified;//修改时间
    private String categories;//文章分类
    private String tags;//文章标签
    private boolean allowComment;//是否允许评论
    private String thumbnail;//文章缩略图
}
